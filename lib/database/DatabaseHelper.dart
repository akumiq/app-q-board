import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {

  static final _databaseName = "mboard.db";
  static final _databaseVersion = 1;

  static final tableStatusTask = 'status_task';
  static final tableProject = 'project';
  static final tablePerson = 'person';

  static final columnId = '_id';
  static final columnPersonId = 'person_id';
  static final columnProjectId = 'project_id';
  static final columnName = 'name';
  static final columnJml = 'jml';
  static final columnTitle = 'title';

  // make this a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $tableStatusTask (
            $columnId INTEGER PRIMARY KEY,
            $columnTitle TEXT NOT NULL,
            $columnProjectId INTEGER NOT NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE $tableProject (
            $columnId INTEGER PRIMARY KEY,
            $columnProjectId INTEGER NOT NULL,
            $columnName TEXT NOT NULL,
            $columnJml TEXT NOT NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE $tablePerson (
            $columnId INTEGER PRIMARY KEY,
            $columnPersonId INTEGER NOT NULL,
            $columnName TEXT NOT NULL,
            $columnJml TEXT NOT NULL
          )
          ''');
  }

  /// DOCUMENT DATABASE
  ///
  /// *
  /// * PERSON CRUD
  /// *
  Future<int> insertPerson(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tablePerson, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRowsPerson() async {
    Database db = await instance.database;
    return await db.query(tablePerson);
  }

  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int> queryRowCountPerson() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tablePerson'));
  }

  Future<List<Map<String, dynamic>>> queryToDetailPerson(String personId) async {
    Database db = await instance.database;
    return await db.rawQuery('SELECT * FROM $tablePerson WHERE $columnPersonId=?', [personId]);
  }

  /// DOCUMENT DATABASE
  ///
  /// *
  /// * PROJECT CRUD
  /// *
  Future<int> insertProject(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tableProject, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRowsProject() async {
    Database db = await instance.database;
    return await db.query(tableProject);
  }

  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int> queryRowCountProject() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tableProject'));
  }

  Future<List<Map<String, dynamic>>> queryToDetailProject(String projectId) async {
    Database db = await instance.database;
    return await db.rawQuery('SELECT * FROM $tableProject WHERE $columnProjectId=?', [projectId]);
  }

  /// DOCUMENT DATABASE
  ///
  /// *
  /// * STATUS_TASK CRUD
  /// *
  Future<int> insertStatusTask(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tableStatusTask, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRowsStatusTask() async {
    Database db = await instance.database;
    return await db.query(tableStatusTask);
  }

  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int> queryRowCountStatusTask() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tableStatusTask'));
  }

  Future<List<Map<String, dynamic>>> queryToDetailStatusTask(String projectId) async {
    Database db = await instance.database;
    return await db.rawQuery('SELECT * FROM $tableStatusTask WHERE $columnProjectId=?', [projectId]);
  }

  // We are assuming here that the id column in the map is set. The other
  // column values will be used to update the row.
  Future<int> updateStatusTask(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int id = row[columnId];
    return await db.update(tableStatusTask, row, where: '$columnId = ?', whereArgs: [id]);
  }

  // Deletes the row specified by the id. The number of affected rows is
  // returned. This should be 1 as long as the row exists.
  Future<int> deleteStatusTask(int id) async {
    Database db = await instance.database;
    return await db.delete(tableStatusTask, where: '$columnId = ?', whereArgs: [id]);
  }
}

class StatusTask {
  final String id;
  final String title;
  final String projectId;

  StatusTask({this.id, this.title, this.projectId});

  factory StatusTask.fromJson(Map<String, dynamic> data) {
    return StatusTask(
        id: data['id'] as String,
        title: data['title'] as String,
        projectId: data['project_id'] as String,
    );
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'StatusTask {id: $id, title: $title, project_id: $projectId}';
  }
}

class DataProject {
  final String id;
  final String projectId;
  final String name;
  final String jml;

  DataProject({this.id, this.projectId, this.name, this.jml});

  factory DataProject.fromJsonProject(Map<String, dynamic> data) {
    return DataProject(
      id: data['id'] as String,
      projectId: data['project_id'] as String,
      name: data['name'] as String,
      jml: data['jml'] as String,
    );
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'Project {id: $id, project_id: $projectId, name: $name, jml: $jml}';
  }
}

class DataPerson {
  final String id;
  final String personId;
  final String name;
  final String jml;

  DataPerson({this.id, this.personId, this.name, this.jml});

  factory DataPerson.fromJsonPerson(Map<String, dynamic> data) {
    return DataPerson(
      id: data['id'] as String,
      personId: data['person_id'] as String,
      name: data['name'] as String,
      jml: data['jml'] as String,
    );
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'DataPerson {id: $id, person_id: $personId, name: $name, jml: $jml}';
  }
}
