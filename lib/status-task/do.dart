import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'custom_edit_status.dart';

class Do extends StatefulWidget {
  @override
  _DoState createState() => _DoState();
}

class _DoState extends State<Do> {
  List data;
  List data2;

  Future getData() async {
    http.Response response = await http.get(
        Uri.encodeFull('http://qodrbee.com/mboard/api.php?nav=rutinitas_admin'),
        headers: {"Accept": "application/json"});
    this.setState(() {
      data2 = json.decode(response.body);
    });
  }

  Future getFilterDo() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            'http://qodrbee.com/mboard/api.php?nav=get_task_by_person_id&person_id=1&filter=do'),
        headers: {"Accept": "application/json"});
    this.setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    this.getData();
    this.getFilterDo();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            _buildListItem(),
            _listBoard(),
          ]),
        )
      ],
    ));
  }

  Widget _buildListItem() {
    return Column(
      children: <Widget>[
        _listTitleBoard('Rutinitas'),
        ListView.builder(
          padding: EdgeInsets.only(top: 8.0),
          itemCount: data2 == null ? 0 : data2.length,
          itemBuilder: (context, index) {
            return _taskRutinitas(index);
          },
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
        ),
      ],
    );
  }

  Widget _listTitleBoard(String title) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }

  Widget _taskRutinitas(int index) {
    return Card(
      child: ListTile(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CustomStatusTask(dataProjectId: '32')));
        },
        contentPadding: EdgeInsets.symmetric(horizontal: 5),
        leading: Icon(
          Icons.stop,
          size: 30,
        ),
        title: Text(data2[index]['title']),
        trailing: Icon(Icons.keyboard_arrow_right),
      ),
    );
  }

  Widget _listBoard() {
    return Column(
      children: <Widget>[
        _listTitleBoard('Do'),
        ListView.builder(
          padding: EdgeInsets.only(top: 8.0),
          itemBuilder: (context, index) {
            return _filterDo(index);
          },
          itemCount: data == null ? 0 : data.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
        ),
      ],
    );
  }

  Widget _filterDo(int index) {
    return Card(
      child: ListTile(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CustomStatusTask(
                      dataProjectId: data[index]['project_id'])));
        },
        contentPadding: EdgeInsets.symmetric(horizontal: 5),
        leading: Icon(
          Icons.stop,
          size: 30,
        ),
        title: Text(data[index]['title']),
        trailing: Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}
