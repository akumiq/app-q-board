import 'package:app_board/database/DatabaseHelper.dart';
import 'package:flutter/material.dart';

class CustomStatusTask extends StatefulWidget {
  final String dataProjectId;

  // receive data from the FirstScreen as a parameter
  CustomStatusTask({Key key, @required this.dataProjectId}) : super(key: key);

  @override
  _CustomStatusTaskState createState() => _CustomStatusTaskState();
}

class _CustomStatusTaskState extends State<CustomStatusTask> {
  List data;
  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Update Status Task'),
      ),
      body: FutureBuilder<List>(
        future: dbHelper.queryToDetailStatusTask(widget.dataProjectId),
        initialData: List(),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (_, int position) {
                    final item = snapshot.data[position];

                    print('dataProjectId row id: ' + widget.dataProjectId);
                    //get your item data here ...
                    return Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Card(
                        child: ListTile(
                          leading: Icon(Icons.keyboard_arrow_left),
                          title: Text(snapshot.data[position]['title']),
                        ),
                      ),
                    );
                  },
                )
              : Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}
