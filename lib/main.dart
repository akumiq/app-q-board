// import 'package:app_board/pages/form-login.dart';
import 'package:app_board/pages/homepage.dart';
import 'package:app_board/pages/login/login_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

final routes = {
  '/login' : (BuildContext context) => LoginPage(),
  '/home' : (BuildContext context) => MyHomePage(),
  '/' : (BuildContext context) => LoginPage(),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: routes,
    );
  }
}
