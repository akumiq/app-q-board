import 'package:app_board/data/data-bottom/project.dart' as allProject;
import 'package:app_board/data/data-bottom/myboard.dart' as myBoard;
import 'package:app_board/data/data-bottom/person.dart' as person;
import 'package:app_board/data/data-bottom/materi.dart' as materi;
import 'package:flutter/material.dart';
import 'package:app_board/database/DatabaseHelper.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  Future<List<DataPerson>> getPerson() async {
    List<DataPerson> list;
    Map<String, dynamic> row;

    try {
      http.Response response = await http.get(
          Uri.encodeFull('http://qodrbee.com/mboard/api.php?nav=person'),
          headers: {"Accept": "application/json"});

      final dataDb = await dbHelper.queryAllRowsPerson();
      if (response.statusCode == 200) {
        if (dataDb.isEmpty) {
          list = parsePersons(response.body);
          for (var i = 0; i < list.length; i++) {
            setState(() {
              row = {
                DatabaseHelper.columnName: list[i].name,
                DatabaseHelper.columnPersonId: list[i].id,
                DatabaseHelper.columnJml: list[i].jml
              };
            });

            final id = await dbHelper.insertPerson(row);
            print('inserted row person: $row');
          }
        }
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List<DataProject>> getProject() async {
    List<DataProject> list;
    Map<String, dynamic> row;

    try {
      http.Response response = await http.get(
          Uri.encodeFull('http://qodrbee.com/mboard/api.php?nav=project'),
          headers: {"Accept": "application/json"});

      final dataDb = await dbHelper.queryAllRowsProject();
      if (response.statusCode == 200) {
        if (dataDb.isEmpty) {
          list = parseProjects(response.body);
          for (var i = 0; i < list.length; i++) {
            setState(() {
              row = {
                DatabaseHelper.columnName: list[i].name,
                DatabaseHelper.columnProjectId: list[i].id,
                DatabaseHelper.columnJml: list[i].jml
              };
            });

            final id = await dbHelper.insertProject(row);
            print('inserted row project: $row');
          }
        }
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List<StatusTask>> getStatusTask() async {
    List<StatusTask> list;
    Map<String, dynamic> row;

    try {
      http.Response response = await http.get(
          Uri.encodeFull('http://qodrbee.com/mboard/api.php?nav=status_task'),
          headers: {"Accept": "application/json"});

      final dataDb = await dbHelper.queryAllRowsStatusTask();
      if (response.statusCode == 200) {
        if (dataDb.isEmpty) {
          list = parseStatusTask(response.body);
          for (var i = 0; i < list.length; i++) {
            setState(() {
              row = {
                DatabaseHelper.columnTitle: list[i].title,
                DatabaseHelper.columnProjectId: list[i].projectId
              };
            });

            final id = await dbHelper.insertStatusTask(row);
            // print('inserted row status_task: $id');
          }
        }
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<StatusTask> parseStatusTask(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<StatusTask>((json) => StatusTask.fromJson(json)).toList();
  }

  static List<DataProject> parseProjects(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed
        .map<DataProject>((json) => DataProject.fromJsonProject(json))
        .toList();
  }

  static List<DataPerson> parsePersons(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed
        .map<DataPerson>((json) => DataPerson.fromJsonPerson(json))
        .toList();
  }

  @override
  void initState() {
    tabController = new TabController(length: 4, vsync: this);
    this.getPerson();
    this.getProject();
    this.getStatusTask();

    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        controller: tabController,
        children: <Widget>[
          myBoard.MyBoard(),
          allProject.Project(),
          person.Person(),
          materi.Materi()
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: 55,
        child: Material(
          color: Colors.blue,
          child: new TabBar(
            controller: tabController,
            tabs: <Widget>[
              new Tab(
                icon: Icon(Icons.home),
                text: "MyBoard",
              ),
              new Tab(
                icon: Icon(Icons.view_module),
                text: "Project",
              ),
              new Tab(
                icon: Icon(Icons.person),
                text: "Person",
              ),
              new Tab(
                icon: Icon(Icons.list),
                text: "Materi",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
