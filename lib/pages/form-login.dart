import 'package:app_board/pages/homepage.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';

const users = const {'miqdad@gmail.com': '123456'};

class FormLogin extends StatelessWidget {
  Duration get loginTime => Duration(milliseconds: 2250);

  Future<String> _authUser(LoginData data) {
    print('Name: ${data.name}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.name)) {
        return 'Username not exists';
      }
      if (users[data.name] != data.password) {
        return 'Password does not match';
      }
      return null;
    });
  }

  Future<String> _recoverPassword(String name) {
    print('Name: $name');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(name)) {
        return 'Username not exists';
      }
      return null;
    });
  }

  // Future<void> _signInAnonymously() async {
  //   try {
  //     await FirebaseAuth.instance.signInAnonymously();
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(title: Text('Sign in')),
  //     body: Center(
  //       child: RaisedButton(
  //         child: Text('Sign in anonymously'),
  //         onPressed: _signInAnonymously,
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      title: 'QODR',
      onLogin: _authUser,
      onSignup: _authUser,
      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => MyHomePage()));
      },
      onRecoverPassword: _recoverPassword,
      messages: LoginMessages(
        usernameHint: "Username"
      ),
    );
  }
}

// class FormLogin extends StatefulWidget {
//   @override
//   _FormLoginState createState() => _FormLoginState();
// }

// class _FormLoginState extends State<FormLogin> with Validation {
//   final formKey = GlobalKey<FormState>();

//   String user = '';
//   String pass = '';

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       // backgroundColor: Colors.lightBlue,
//       body: Padding(
//         padding: const EdgeInsets.all(10),
//         child: Form(
//           key: formKey,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text(
//                 'Form Login',
//                 style: TextStyle(fontSize: 25),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 20),
//               ),
//               TextFormField(
//                 validator: validateUser,
//                 onSaved: (String value) {
//                   user = value;
//                 },
//                 decoration: InputDecoration(
//                     hintText: 'Username',
//                     labelText: 'Username',
//                     border: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(10),
//                     )),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 10),
//               ),
//               TextFormField(
//                 validator: validatePass,
//                 onSaved: (String value) {
//                   pass = value;
//                 },
//                 decoration: InputDecoration(
//                     hintText: 'Password',
//                     labelText: 'Password',
//                     border: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(10))),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 30),
//                 child: RaisedButton(
//                   color: Colors.blueGrey,
//                   onPressed: () {
//                     if (formKey.currentState.validate()) {
//                       formKey.currentState.save();

//                       print("Username : $user");
//                       print("Password : $pass");
//                     }

//                     Navigator.push(context,
//                         MaterialPageRoute(builder: (context) => MyHomePage()));
//                   },
//                   child: Text('Login'),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
