import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRangePicker;
import 'package:app_board/database/DatabaseHelper.dart';
import 'dart:async';

class AddTask extends StatefulWidget {
  @override
  _AddTaskState createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  final cTitle = TextEditingController();
  final cDesc = TextEditingController();

  DateTime selectDate = DateTime.now();
  DateTime lastDate = DateTime.now().add(Duration(days: 14));

  DatabaseHelper dbHelper = DatabaseHelper.instance;

  String _currentUser;

  List<String> progression = ["B.log", "Do", "Done", "All"];
  String _progression = "B.log";

  void _chooseProgression(String value) {
    setState(() {
      _progression = value;
    });
  }

  Future rangeDate(BuildContext context) async {
    final List<DateTime> rangePick = await DateRangePicker.showDatePicker(
        context: context,
        initialFirstDate: selectDate,
        initialLastDate: lastDate,
        firstDate: DateTime.now().add(Duration(days: -365)),
        lastDate: DateTime.now().add(Duration(days: 365)));

    if (rangePick != null && rangePick.length == 2) {
      setState(() {
        selectDate = rangePick[0];
        lastDate = rangePick[1];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Add New Task',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 1.5,
          actions: <Widget>[
            IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(
                Icons.close,
                color: Colors.black,
                size: 25.0,
              ),
            )
          ],
        ),
        body: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  FutureBuilder(
                    future: dbHelper.queryAllRowsProject(),
                    initialData: List(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return DropdownButton<String>(
                          items: snapshot.data
                              .map<DropdownMenuItem<String>>(
                                  (user) => DropdownMenuItem<String>(
                                        child: Text(user['name']),
                                        value: user['name'],
                                      ))
                              .toList(),
                          onChanged: (String value) {
                            setState(() {
                              _currentUser = value;
                            });
                          },
                          isExpanded: true,
                          // value: _currentUser,
                          hint: Center(child: Text('Choose The Project')),
                        );
                      }
                      return CircularProgressIndicator();
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0),
                    child: TextFormField(
                      controller: cTitle,
                      decoration: InputDecoration(
                          hintText: 'Title',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10))),
                      style: TextStyle(fontSize: 20),
                      maxLines: null,
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: TextFormField(
                      controller: cDesc,
                      decoration: InputDecoration(
                          hintText: 'Description',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10))),
                      style: TextStyle(fontSize: 20),
                      maxLines: null,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.newline,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0),
                    child: FlatButton(
                      onPressed: () async {
                        rangeDate(context);
                      },
                      child: Text('Select Date'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Flexible(
                            child: Container(
                                padding: EdgeInsets.all(18),
                                decoration: BoxDecoration(
                                    border: Border.all(width: 3),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(DateFormat("dd-MM-yyyy")
                                    .format(selectDate)))),
                        Padding(
                          padding: const EdgeInsets.all(10),
                        ),
                        Flexible(
                            child: Container(
                                padding: EdgeInsets.all(18),
                                decoration: BoxDecoration(
                                    border: Border.all(width: 3),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(DateFormat("dd-MM-yyyy")
                                    .format(lastDate)))),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                            child: DropdownButton(
                          onChanged: (String value) {
                            _chooseProgression(value);
                          },
                          value: _progression,
                          items: progression.map((String value) {
                            return DropdownMenuItem(
                                value: value,
                                child: Center(
                                    child: Text(
                                  value,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                )));
                          }).toList(),
                          isExpanded: true,
                        )),
                        Padding(
                          padding: const EdgeInsets.all(5),
                        ),
                        Flexible(
                          child: FutureBuilder(
                            future: dbHelper.queryAllRowsPerson(),
                            initialData: List(),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return DropdownButton<String>(
                                  items: snapshot.data
                                      .map<DropdownMenuItem<String>>(
                                          (user) => DropdownMenuItem<String>(
                                                child: Text(user['name']),
                                                value: user['name'],
                                              ))
                                      .toList(),
                                  onChanged: (String value) {
                                    setState(() {
                                      _currentUser = value;
                                    });
                                  },
                                  isExpanded: true,
                                  // value: _currentUser,
                                  hint: Center(child: Text('assignee')),
                                );
                              }
                              return CircularProgressIndicator();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Container(
                      height: 40,
                      width: 250,
                      child: RaisedButton(
                        onPressed: null,
                        shape: StadiumBorder(side: BorderSide.none),
                        color: Colors.blueAccent,
                        child: Text('SAVE DATA'),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
