import 'package:app_board/data/data-login/rest_data.dart';
import 'package:app_board/data/data-login/user.dart';
abstract class LoginPageContract {
  void onLoginSuccess(User user);
  void onLoginError(String errorTxt);
}

class LoginPagePresenter {
  LoginPageContract _view;
  RestData api = RestData();
  LoginPagePresenter(this._view);

  doLogin(String username, String password) {
    api
        .login(username, password)
        .then((User user) => _view.onLoginSuccess(user))
        .catchError((onError) => _view.onLoginError(onError));
  }
}