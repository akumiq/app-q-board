import 'package:flutter/material.dart';

class ListTitle extends StatefulWidget {
  @override
  _ListTitleState createState() => _ListTitleState();
}

class _ListTitleState extends State<ListTitle> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        title: Text('Persons'),
      ),
      body: Padding(
          padding: const EdgeInsets.only(left: 8, top: 10, right: 8),
          child: Column(
            children: <Widget>[
              CustomListTitle(
                user: 'Miqdad',
                countTask: '2/4',
              ),
              CustomListTitle(
                user: 'Farhan',
                countTask: '6/10',
              ),
              CustomListTitle(
                user: 'Abim',
                countTask: '5/7',
              )
            ],
          )),
    );
  }
}

class CustomListTitle extends StatelessWidget {
  final String user;
  final String countTask;

  CustomListTitle({this.user, this.countTask});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: null,
        // onTap: () => Navigator.push(
        //     context, MaterialPageRoute(builder: (context) => ListPerson())),
        // leading: Icon(Icons.person),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              user,
              style: TextStyle(fontSize: 18),
            ),
            Text(countTask)
          ],
        ),
        trailing: Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}