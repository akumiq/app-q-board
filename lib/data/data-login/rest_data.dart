import 'package:app_board/data/data-login/user.dart';
import 'package:app_board/utils/network_util.dart';
import 'package:app_board/database/DBHelper.dart';
import 'dart:async';

class RestData {
  DatabaseHelper db = DatabaseHelper();

  NetworkUtil _netUtil = NetworkUtil();

  static final BASE_URL = "";
  static final LOGIN_URL = BASE_URL + "/";

  Future<User> login(String username, String password) {
    return Future.value(User(username, password));
  }
}