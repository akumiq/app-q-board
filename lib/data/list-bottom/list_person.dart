import 'package:app_board/status-task/custom_edit_status.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ListPerson extends StatefulWidget {
  final String dataPersonId;

  // receive data from the FirstScreen as a parameter
  ListPerson({Key key, @required this.dataPersonId}) : super(key: key);

  @override
  _ListPersonState createState() => _ListPersonState();
}

class _ListPersonState extends State<ListPerson> {
  List data;

  Future getProjectById() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            'http://qodrbee.com/mboard/api.php?nav=get_task_by_person_id&person_id=' +
                widget.dataPersonId),
        headers: {"Accept": "application/json"});
    this.setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    this.getProjectById();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Task By Person'),
      ),
      body: ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CustomStatusTask(
                            dataProjectId: data[index]['project_id'])));
              },
              contentPadding: EdgeInsets.symmetric(horizontal: 15),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Flexible(
                    child: new Container(
                      padding: new EdgeInsets.only(right: 13.0),
                      child: new Text(
                        data[index]['title'],
                        overflow: TextOverflow.ellipsis,
                        style: new TextStyle(
                          fontSize: 15.0,
                          fontFamily: 'Roboto',
                          color: new Color(0xFF212121),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
//                  Text(data[index]['description'])
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          );
        },
      ),
    );
  }
}
