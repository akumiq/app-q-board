import 'package:app_board/pages/addtask.dart';
import 'package:app_board/status-task/all.dart';
import 'package:app_board/status-task/b-log.dart';
import 'package:app_board/status-task/do.dart';
import 'package:app_board/status-task/done.dart';
import 'package:app_board/data/data-login/auth.dart';
import 'package:flutter/material.dart';

class MyBoard extends StatefulWidget {
  final AuthStateListener authStateListener;
  final AuthStateProvider authStateProvider;

  MyBoard({this.authStateListener, this.authStateProvider});

  @override
  _MyBoardState createState() => _MyBoardState();
}

class _MyBoardState extends State<MyBoard> {
  PageController _pageController = PageController();

  double currentPage = 0;
  double currentPage2 = 1;

  void _signOut() async {
    try {
      widget.authStateProvider.notify(AuthState.LOGGED_OUT);
      widget.authStateListener.onAuthStateChanged(AuthState.LOGGED_OUT);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page;
      });
    });

    return Scaffold(
      drawer: Container(
        width: 200,
        child: Drawer(
          child: ListView(
            children: <Widget>[
              Container(
                height: 70,
                child: DrawerHeader(
                  child: Center(
                      child: Text(
                    'QODR',
                    style: TextStyle(fontSize: 25),
                  )),
                  decoration: BoxDecoration(color: Colors.blue),
                ),
              ),
              SizedBox(
                height: 450,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                leading: Icon(Icons.exit_to_app),
                title: Text(
                  'Log Out',
                  style: TextStyle(fontSize: 18),
                ),
                onTap: _signOut,
              ),
            ],
          ),
        ),
      ),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          title: Text('My Board'),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: FlatButton.icon(
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddTask())),
                color: Colors.blue[800],
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                icon: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
                label: Text(
                  'Task',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(
                'Nikmati Hari Ini dengan Bersyukur',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: _listButtonStatus(context),
            ),
            Expanded(
                child: PageView(
              controller: _pageController,
              children: <Widget>[BLog(), Do(), Done(), All()],
            ))
          ],
        ),
      ),
    );
  }

  Widget _listButtonStatus(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: MaterialButton(
            onPressed: () {
              _pageController.previousPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.bounceInOut);
            },
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: currentPage < 0.5
                        ? Colors.transparent
                        : Theme.of(context).accentColor),
                borderRadius: BorderRadius.circular(12)),
            color: currentPage < 0.5
                ? Theme.of(context).accentColor
                : Colors.white,
            textColor: currentPage < 0.5
                ? Colors.white
                : Theme.of(context).accentColor,
            child: Text('B.Log'),
          ),
        ),
        Expanded(
          child: MaterialButton(
            onPressed: () {
              _pageController.nextPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.bounceInOut);
            },
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: currentPage == 1
                        ? Colors.transparent
                        : Theme.of(context).accentColor),
                borderRadius: BorderRadius.circular(12)),
            color:
                currentPage == 1 ? Theme.of(context).accentColor : Colors.white,
            textColor:
                currentPage == 1 ? Colors.white : Theme.of(context).accentColor,
            child: Text('Do'),
          ),
        ),
        Expanded(
          child: MaterialButton(
            onPressed: () {
              _pageController.nextPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.bounceInOut);
            },
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: currentPage == 2
                        ? Colors.transparent
                        : Theme.of(context).accentColor),
                borderRadius: BorderRadius.circular(12)),
            color:
                currentPage == 2 ? Theme.of(context).accentColor : Colors.white,
            textColor:
                currentPage == 2 ? Colors.white : Theme.of(context).accentColor,
            child: Text('Done'),
          ),
        ),
        Expanded(
          child: MaterialButton(
            onPressed: () {
              _pageController.nextPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.bounceInOut);
            },
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: currentPage >= 2.5
                        ? Colors.transparent
                        : Theme.of(context).accentColor),
                borderRadius: BorderRadius.circular(12)),
            color: currentPage >= 2.5
                ? Theme.of(context).accentColor
                : Colors.white,
            textColor: currentPage >= 2.5
                ? Colors.white
                : Theme.of(context).accentColor,
            child: Text('All'),
          ),
        )
      ],
    );
  }
}
