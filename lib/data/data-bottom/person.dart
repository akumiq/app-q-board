// import 'package:app_q_board/data/list-bottom/list_person.dart';
import 'package:app_board/data/list-bottom/list_person.dart';
import 'package:app_board/database/DatabaseHelper.dart';
import 'package:flutter/material.dart';

class Person extends StatefulWidget {
  @override
  _PersonState createState() => _PersonState();
}

class _PersonState extends State<Person> {
  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Person'),
      ),
      body: FutureBuilder<List>(
        future: dbHelper.queryAllRowsPerson(),
        initialData: List(),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (_, int position) {
              final item = snapshot.data[position];
              //get your item data here ...
              return Card(
                child: ListTile(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ListPerson(dataPersonId: item['person_id'].toString())));
                  },
                  contentPadding: EdgeInsets.symmetric(horizontal: 15),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        snapshot.data[position]['name'],
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(snapshot.data[position]['jml'])
                    ],
                  ),
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              );
            },
          )
              : Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
