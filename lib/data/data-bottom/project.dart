import 'package:app_board/data/list-bottom/list_project.dart';
import 'package:app_board/database/DatabaseHelper.dart';
import 'package:flutter/material.dart';

class Project extends StatefulWidget {
  @override
  _ProjectState createState() => _ProjectState();
}

class _ProjectState extends State<Project> {
  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Project'),
      ),
      body: FutureBuilder<List>(
        future: dbHelper.queryAllRowsProject(),
        initialData: List(),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (_, int position) {
              final item = snapshot.data[position];

              //get your item data here ...
              return Card(
                child: ListTile(
                  onTap: () {
                     Navigator.push(context,
                       MaterialPageRoute(builder: (context) => ListProject(dataProjectId: item['project_id'].toString())));
                  },
                  contentPadding: EdgeInsets.symmetric(horizontal: 15),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        snapshot.data[position]['name'],
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(snapshot.data[position]['jml'])
                    ],
                  ),
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              );
            },
          )
              : Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
