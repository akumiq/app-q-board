import 'package:flutter/material.dart';

class Materi extends StatefulWidget {
  @override
  _MateriState createState() => _MateriState();
}

class _MateriState extends State<Materi> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: choices.length);

    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();

    super.dispose();
  }

  void _nextPage(int delta) {
    final int newIndex = _tabController.index + delta;

    if (newIndex < 0 || newIndex >= _tabController.length) return;

    _tabController.animateTo(newIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Materi'),
        leading: IconButton(
          onPressed: () {
            _nextPage(-1);
          },
          icon: Icon(Icons.arrow_back),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _nextPage(1);
            },
            icon: Icon(Icons.arrow_forward),
          )
        ],
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(30),
          child: Theme(
            data: Theme.of(context).copyWith(accentColor: Colors.white),
            child: Container(
              height: 30,
              alignment: Alignment.center,
              child: TabPageSelector(
                controller: _tabController,
              ),
            ),
          ),
        ),
      ),
      body: TabBarView(
          controller: _tabController,
          children: choices.map((Choice choice) {
            return Padding(
              padding: const EdgeInsets.all(15),
              child: ChoiceCard(
                choice: choice,
              ),
            );
          }).toList()),
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: "Car", icon: Icons.directions_car),
  const Choice(title: "Bus", icon: Icons.directions_bus),
  const Choice(title: "Train", icon: Icons.directions_railway)
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice}) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;

    return Card(
      color: Colors.blue,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 100, color: textStyle.color),
            Text(choice.title, style: textStyle)
          ],
        ),
      ),
    );
  }
}
