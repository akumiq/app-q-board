import 'package:flutter/material.dart';

class CustomDatePicker extends StatelessWidget {
  final VoidCallback onPressed;
  final String value;

  CustomDatePicker({@required this.onPressed, @required this.value});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
          onPressed: onPressed,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Row(
              children: <Widget>[
                // Icon(icon, color: Theme.of(context).accentColor, size: 30),
                // SizedBox(
                //   width: 12,
                // ),
                Text(
                  value,
                  style: TextStyle(fontSize: 14),
                )
              ],
            ),
          ),
        ),
        FlatButton(
          onPressed: onPressed,
          child: Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Row(
              children: <Widget>[
                // Icon(icon, color: Theme.of(context).accentColor, size: 30),
                // SizedBox(
                //   width: 12,
                // ),
                Text(
                  value,
                  style: TextStyle(fontSize: 14),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
